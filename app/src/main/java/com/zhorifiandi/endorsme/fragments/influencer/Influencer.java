package com.zhorifiandi.endorsme.fragments.influencer;

/**
 * Created by zhorifiandi on 11/12/17.
 */

import com.zhorifiandi.endorsme.R;

import java.util.Random;

public class Influencer {

    private static final Random RANDOM = new Random();

    public static int getRandomCheeseDrawable() {
        switch (RANDOM.nextInt(5)) {
            default:
            case 0:
                return R.drawable.influencer1;
            case 1:
                return R.drawable.influencer2;
            case 2:
                return R.drawable.influencer3;
            case 3:
                return R.drawable.influencer4;
            case 4:
                return R.drawable.influencer5;
        }
    }

    public static final String[] list_influencers = {
            "Anya","Tasya", "Vita", "Aldi", "Riska"
    };

    public static int getCheeseDrawable(int position) {
        switch (position) {
            default:
            case 0:
                return R.drawable.influencer1;
            case 1:
                return R.drawable.influencer2;
            case 2:
                return R.drawable.influencer3;
            case 3:
                return R.drawable.influencer4;
            case 4:
                return R.drawable.influencer5;
        }
    }
}
