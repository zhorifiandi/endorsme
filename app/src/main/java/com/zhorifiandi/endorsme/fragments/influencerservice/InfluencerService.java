package com.zhorifiandi.endorsme.fragments.influencerservice;

/**
 * Created by zhorifiandi on 11/12/17.
 */

import com.zhorifiandi.endorsme.R;

import java.util.Random;

public class InfluencerService {

    private static final Random RANDOM = new Random();

    public static int getRandomCheeseDrawable() {
        switch (RANDOM.nextInt(5)) {
            default:
            case 0:
                return R.drawable.service1;
            case 1:
                return R.drawable.service2;
            case 2:
                return R.drawable.service3;
        }
    }

    public static final String[] serviceList = {
        "Instagram Photo Post", "Instagram Video Post", "Instagram Story"
    };



    public static int getServiceDrawable(int position) {
        switch (position) {
            default:
            case 0:
                return R.drawable.service1;
            case 1:
                return R.drawable.service2;
            case 2:
                return R.drawable.service3;
        }
    }
}
